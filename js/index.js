const title = document.querySelector('.title');
const countriesSelect = document.getElementById('countries');
const BASE_URL = 'https://corona-api.com';

let error = null;
const errorDiv = document.querySelector('.error');
let info = '';
let infoDiv = document.querySelector('.info');
let countryStatsDiv = document.querySelector('.country-stats');
var countryName;
var myChart;

countriesSelect.addEventListener('change', evt => {
    const countryCode = evt.target.value;
    const countryLabel = evt.target.selectedOptions[0].label;
    getStatistics(countryCode)
        .then(stats => {
            displayStatistics(stats, countryCode);
        })
        .then(() => {
            displayDailyStatistics(countryCode, countryLabel);
        })
        .catch(err => {
            countryStatsDiv.innerHTML = '';
            // canvasDiv.innerHTML = '';
            // countryCardDiv.innerHTML = '';
            errorDiv.innerHTML = err.message;
        });
})

window.addEventListener('load', evt => {
    initStatistics()
        .then(stats => {
            displayStatistics(stats);
        })
        .then(() => {
            displayDailyStatistics();
        })
        .catch(err => {
            countryStatsDiv.innerHTML = '';
            // canvasDiv.innerHTML = '';
            // countryCardDiv.innerHTML = '';
            errorDiv.innerHTML = err.message;
        });
})

function getCountries() {
    return new Promise((resolve, reject) => {
        fetch(`${BASE_URL}/countries`)
            .then(data => data.json())
            .then(countries => {
                // console.log('countries', countries);
                resolve(countries);
            })
            .catch(err => {
                reject(err);
                errorDiv.innerHTML('Impossible de récupérer la liste de pays');
            })
    })
}

function compare(a, b) {
    const countryA = a.name.toUpperCase();
    const countryB = b.name.toUpperCase();
  
    let comparison = 0;
    if (countryA > countryB) {
      comparison = 1;
    } else if (countryA < countryB) {
      comparison = -1;
    }
    return comparison;
  }
  

getCountries().then(data => {
    let option;
    Object.entries(data.data.sort(compare)).map(country => {
        option = document.createElement('option');
        option.text = country[1].name;
        option.value = country[1].code;
        countriesSelect.add(option);
        return undefined;
    })
})

function getStatistics(countryCode) {
    return new Promise((resolve, reject) => {
        let path;
        if (countryCode == 'all') {
            path = `${BASE_URL}/timeline`;
        } else {
            path = `${BASE_URL}/countries/${countryCode}`;
        }
        fetch(path)
            .then(data => data.json())
            .then(stats => {
                // console.log('stats', stats);
                if (stats.error) {
                    throw Error(stats.error.message);
                }
                resolve(stats);
            })
            .catch(err => {
                reject(err);
                errorDiv.innerHTML(`Impossible d'afficher les statistiques pour ${countryCode}`);
            })
    })
}

function displayStatistics(stats, countryCode) {

    let stat = {};
    let lastUpdate = '';
    let sep = '';
    let percentActive = 0;
    let percentRecovered = 0;
    let percentDead = 0;
    let percentCritical = '';

    try {
        if (!countryCode || countryCode === 'all') {
            stat = stats.data[0];
            stat.critical = '';
            lastUpdate = new Date(stat.updated_at);
            percentCritical = '';
        } else {
            stat = stats.data.timeline[0];
            stat.critical = stats.data.latest_data.critical;
            sep = '|';
            lastUpdate = new Date(stats.data.timeline[0].updated_at);
        }
    } catch(error) {
        console.log('error', error.message);
    }

    let countryStats = '';
    if (stat) {
        const niceDate = getLastDataUpdateDate(lastUpdate);

        if (stat.confirmed !== 0) {
            percentActive = (100*stat.active/stat.confirmed).toFixed(0);
            percentRecovered = (100*stat.recovered/stat.confirmed).toFixed(0);
            percentDead = (100*stat.deaths/stat.confirmed).toFixed(0);
            if (stat.active !== 0) {
                percentCritical = (100*stat.critical/stat.active).toFixed(0);
            }
            if (!countryCode || countryCode === 'all') {
                percentCritical = '';
            } else {
                percentCritical += '%';
            }
            
        }
        errorDiv.innerHTML = ''; 
        countryStats = `
            <div class="row">
                <div class="col-sm-3">
                    <div class="card text-white shadow-sm mb-2 bg-primary">
                        <div class="card-header">Confirmés</div>
                        <div class="card-body">
                            <h5 class="card-title">${stat.confirmed.toLocaleString()}</h5>
                            <p class="card-text text-right">N/A</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card text-white shadow-sm mb-2 bg-warning">
                        <div class="card-header">Actifs | Critiques</div>
                        <div class="card-body">
                            <h5 class="card-title">${stat.active.toLocaleString()} ${sep} ${stat.critical.toLocaleString()}</h5>
                            <p class="card-text text-right">${percentActive}% ${sep} ${percentCritical}</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card text-white shadow-sm mb-2 bg-success">
                        <div class="card-header">Guéris</div>
                        <div class="card-body">
                            <h5 class="card-title">${stat.recovered.toLocaleString()}</h5>
                            <p class="card-text text-right">${percentRecovered}%</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="card text-white shadow-sm mb-2 bg-danger">
                    <div class="card-header">Décédés</div>
                        <div class="card-body">
                            <h5 class="card-title">${stat.deaths.toLocaleString()}</h5>
                            <p class="card-text text-right">${percentDead}%</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">Données mise à jour le ${niceDate}</div>
            </div>
        `;
    } else {
        countryStats = `
            <div class="row">
                <div class="col-sm-12">
                    <h2>Pas de données valides</h2>
                </div>
            </div>
        `;
    }

    countryStatsDiv.innerHTML = countryStats;
}

function getLastDataUpdateDate(lastUpdate) {
    return lastUpdate.toLocaleString();
}

function initStatistics() {
    return new Promise((resolve, reject) => {
        fetch(`${BASE_URL}/timeline`)
            .then(data => data.json())
            .then(stats => {
                // console.log('stats', stats);
                if (stats.error) {
                    throw Error(stats.error.message);
                }
                resolve(stats);
            })
            .catch(err => {
                reject(err);
                errorDiv.innerHTML(`Impossible d'afficher les statistiques pour ${countryCode}`);
            })
    })
}

function getDailyStatistics(countryCode, countryLabel) {
    if (countryCode !== 'all' && countryCode) {
        return new Promise((resolve, reject) => {
            fetch(`${BASE_URL}/countries/${countryCode}`)
                .then(data => data.json())
                .then(stats => {
                    if (stats.error) {
                        throw Error(stats.error.message);
                    }
                    let dailyStats = stats.data.timeline.map(stat => {
                        if (!stat.is_in_progress) {
                            return { "date": stat.date,
                                    "confirmed": stat.new_confirmed,
                                    "dead": stat.new_deaths };
                        }
                    });
                    // console.log('dailystats', dailyStats);
                    resolve(dailyStats);
                })
                .catch(err => {
                    reject(err);
                    errorDiv.innerHTML(`Impossible d'afficher les statistiques quotidiennes`);
                })
        })
    } else {
        return new Promise((resolve, reject) => {
            fetch(`${BASE_URL}/timeline`)
                .then(data => data.json())
                .then(stats => {
                    if (stats.error) {
                        throw Error(stats.error.message);
                    }
                    let dailyStats = stats.data.map(stat => {
                        return { "date": stat.date,
                        "confirmed": stat.new_confirmed,
                        "dead": stat.new_deaths };
                    });
                    // console.log('dailystats', dailyStats);
                    resolve(dailyStats);
                })
                .catch(err => {
                    reject(err);
                    errorDiv.innerHTML(`Impossible d'afficher les statistiques quotidiennes`);
                })
        })
    }
}

function displayDailyStatistics(countryCode, countryLabel) {
    const dailyStats = getDailyStatistics(countryCode, countryLabel);
    let dataConfirmed = [];
    let dataDead = [];
    // let dataTime = [];
    const ctx = document.getElementById('myChart');
    dailyStats
    .then(function(value) {
        // console.log('value', value);
        Object.entries(value).map((stat) => {
            stat[1]?.confirmed && dataConfirmed.push({'x': stat[1].date, 'y': stat[1].confirmed});
            stat[1]?.dead && dataDead.push({'x': stat[1].date, 'y': stat[1].dead});
            // dataTime.push(stat[1].date);
            return undefined;
        });
    })
    .then(() => {
        // console.log('dataConfirmed', dataConfirmed);
        // console.log('dataDead', dataDead);
        // console.log('dataTime', dataTime);
        // console.log('dataDeltaConfirmed', dataDeltaConfirmed);
        // console.log('dataDeltaDead', dataDeltaDead);
        if (myChart) {
            myChart.destroy();
        }
        if (!dataConfirmed.length && !dataDead.length) { 
            document.getElementById("myChart").hidden = true;
        } else {
            document.getElementById("myChart").hidden = false;
        }

        myChart = new Chart(ctx, {
            type: 'line',
            data: {
                datasets: [{
                    data: dataConfirmed,
                    label: 'Cas confirmés',
                    backgroundColor: 'rgba(0, 123, 255, 0.2)', 
                    borderColor: 'rgba(0, 123, 255, 1)',    
                    // This binds the dataset to the left y axis
                    yAxisID: 'left-y-axis',
                    cubicInterpolationMode: 'monotone'
                }, {
                    data: dataDead,
                    label: 'Cas décédés',
                    backgroundColor: 'rgba(220, 53, 69, 0.2)', 
                    borderColor: 'rgba(220, 53, 69, 1)', 
                    // This binds the dataset to the right y axis
                    yAxisID: 'right-y-axis',
                    cubicInterpolationMode: 'monotone'
                }]
            },
            options: {
                responsive: true,
                title:      {
                    display: true,
                    text:    "Evolution du Covid 19"
                },
                scales: {
                    xAxes: [{
                        type: 'time',
                        time: {
                            parser: 'YYYY-MM-DD',
                            tooltipFormat: 'll',
                            displayFormats: {
                                day: 'D MMM'
                            }
                        },
                        scaleLabel: {
                            display: true,
                            labelString: 'Date'
                        }
                    }],
                    yAxes: [{
                        id: 'left-y-axis',
                        type: 'linear',
                        position: 'left',
                        ticks:{
                            callback: function (value) {
                              return Number(value).toLocaleString()
                            }
                        },
                    }, {
                        id: 'right-y-axis',
                        type: 'linear',
                        position: 'right',
                        ticks:{
                            callback: function (value) {
                              return Number(value).toLocaleString()
                            }
                        },
                    }]
                }
            }
        });     
    });

    // console.log('dataConfirmed', dataConfirmed);
    

}